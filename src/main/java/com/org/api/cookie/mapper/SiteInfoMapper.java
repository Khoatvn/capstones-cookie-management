package com.org.api.cookie.mapper;

import com.org.api.cookie.model.dataset.SiteCreateDataset;
import com.org.api.cookie.model.dataset.SiteInfoDataset;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * SiteInfoMapper
 *
 * @since 2021/04/25
 */
@Mapper
public interface SiteInfoMapper {

    @Insert("INSERT INTO cc_site_info (" +
            "   id, " +
            "   user_id, " +
            "   domain, " +
            "   end_point_url, " +
            "   status " +
            ") VALUES ( " +
            "   #{id}, " +
            "   #{userId}, " +
            "   #{domain}, " +
            "   #{endPointURL}, " +
            "   #{status} " +
            ")")
    Integer insSiteInfo(SiteCreateDataset siteCreateDataset);

    @Select("SELECT " +
            "   id, " +
            "   user_id AS userId, " +
            "   domain AS domain, " +
            "   end_point_url AS endPointUrl, " +
            "   status AS status " +
            "FROM " +
            "   cc_site_info " +
            "WHERE " +
            "   user_id = #{userId};")
    List<SiteInfoDataset> selSiteInfoLst(@Param("userId") String userId);

    @Select("SELECT COUNT(1) FROM cc_site_info WHERE domain = #{domain} AND user_id = #{userId}")
    Integer selExistDomain(@Param("userId") String userId, @Param("domain") String domain);

    @Select("SELECT COUNT(1) FROM cc_site_info WHERE end_point_url = #{endPointURL}")
    Integer selExistServerAlias(@Param("endPointURL") String endPointURL);

}
