package com.org.api.cookie.mapper;

import com.org.api.cookie.model.dataset.UserCreateDataset;
import com.org.api.cookie.model.dataset.UserDataset;
import com.org.api.cookie.model.dataset.UserPasswordDataset;
import com.org.api.cookie.model.dataset.UserServerDataset;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * UserMapper
 *
 * @since 2021/04/25
 */
@Mapper
public interface UserMapper {

    @Update("UPDATE " +
            "   cc_user " +
            "SET " +
            "   password = #{newPassword} " +
            "WHERE " +
            "   id = #{id} " +
            "   AND password = #{oldPassword}")
    Integer updPassword(UserPasswordDataset userPasswordDataset);

    @Update("UPDATE " +
            "   cc_user " +
            "SET " +
            "   status = #{status} " +
            "WHERE " +
            "   id = #{id} ")
    Integer updStatus(@Param("id") String id,
                      @Param("status") String status);

    /**
     * Select user information for authentication by username and password
     * @param username
     * @param password
     * @return UserDataset
     */
    @Select("SELECT " +
            "   id, " +
            "   username, " +
            "   password," +
            "   role_id AS roleId " +
            "FROM " +
            "   cc_user " +
            "WHERE " +
            "   username = #{username} " +
            "   AND password = #{password}" +
            "   AND status = '0'; ")
    UserDataset selUserInfoForAuth(@Param("username") String username,
                                   @Param("password") String password);

    /**
     * Select all user
     * @return UserDataset
     */
    @Select("SELECT " +
            "   ccu.id AS id, " +
            "   ccu.username, " +
            "   ccu.full_name AS fullName, " +
            "   ccu.phone_number AS phoneNumber, " +
            "   ccu.status AS status, " +
            "   ccu.role_id AS roleId " +
            "FROM " +
            "   cc_user ccu " +
            "WHERE " +
            "   ccu.role_id != '0' ")
    List<UserServerDataset> selAllUser();

    /**
     * Select user information by user's id
     * @param id
     * @return UserDataset
     */
    @Select("SELECT " +
            "   ccu.id AS id, " +
            "   ccu.username, " +
            "   ccu.full_name AS fullName, " +
            "   ccu.phone_number AS phoneNumber, " +
            "   ccu.status AS status, " +
            "   ccu.role_id AS roleId " +
            "FROM " +
            "   cc_user ccu " +
            "WHERE " +
            "   ccu.id = #{id} ")
    UserServerDataset selUserInfo(@Param("id") String id);

    /**
     * Select user information by user's id
     * @param id
     * @return UserDataset
     */
    @Select("SELECT " +
            "   id, " +
            "   full_name AS fullName, " +
            "   username, " +
            "   phone_number AS phoneNumber, " +
            "   status AS status " +
            "FROM " +
            "   cc_user " +
            "WHERE " +
            "   id = #{id} ")
    UserDataset selUserInfoComm(@Param("id") String id);

    /**
     * Insert user information
     * @param userCreateDataset
     * @return Integer
     */
    @Insert("INSERT INTO cc_user ( " +
            "   id, " +
            "   username, " +
            "   password, " +
            "   full_name, " +
            "   phone_number, " +
            "   role_id, " +
            "   status " +
            ") VALUES ( " +
            "   #{id}, " +
            "   #{username}, " +
            "   #{password}, " +
            "   #{fullName}, " +
            "   #{phoneNumber}, " +
            "   #{roleId}, " +
            "   #{status} " +
            ")")
    Integer insUser(UserCreateDataset userCreateDataset);

    @Select("SELECT COUNT(1) FROM cc_user WHERE username = #{username}")
    Integer selExistUsername(@Param("username") String username);

}
