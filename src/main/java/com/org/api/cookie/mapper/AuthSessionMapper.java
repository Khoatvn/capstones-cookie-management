package com.org.api.cookie.mapper;

import com.org.api.cookie.model.dataset.AuthSessionDataset;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * AuthSessionMapper
 *
 *
 * @since 2020/11/14
 */
@Mapper
public interface AuthSessionMapper {

    /**
     * Select session authentication information
     * @param userId
     * @param sessionId
     * @return AuthSessionDataset
     */
    @Select("SELECT " +
            "   ccas.user_id AS userId, " +
            "   ccas.session_id AS sessionId, " +
            "   ccas.expiration_time AS expirationTime, " +
            "   ccas.auth_key AS authKey, " +
            "   ccas.timeout_flg AS timeoutFlg, " +
            "   ccas.login_time AS loginTime, " +
            "   ccas.logout_time AS logoutTime " +
            "FROM " +
            "   cc_auth_session ccas " +
            "INNER JOIN " +
            "   cc_user ccu " +
            "   ON " +
            "       ccas.user_id = ccu.id " +
            "       AND ccas.auth_key = ccu.password " +
            "WHERE " +
            "   ccas.user_id = #{userId} " +
            "   AND ccu.status = '0' " +
            "   AND ccas.session_id = #{sessionId} " +
            "   AND ccas.timeout_flg != '1'; ")
    AuthSessionDataset selAuthSessionInfo(@Param("userId") String userId,
                                          @Param("sessionId") String sessionId);

    /**
     * Insert authentication session information
     * @param authSessionDataset
     * @return Integer
     */
    @Insert("INSERT INTO cc_auth_session ( " +
            "   user_id, " +
            "   session_id, " +
            "   expiration_time, " +
            "   auth_key, " +
            "   timeout_flg, " +
            "   login_time, " +
            "   logout_time " +
            ") VALUES ( " +
            "   #{userId}, " +
            "   #{sessionId}, " +
            "   #{expirationTime}, " +
            "   #{authKey}, " +
            "   #{timeoutFlg}, " +
            "   #{loginTime}, " +
            "   #{logoutTime} " +
            "); ")
    Integer insAuthSessionInfo(AuthSessionDataset authSessionDataset);

    /**
     * Expire all session by user id
     * @param userId
     * @return Integer
     */
    @Insert("UPDATE " +
            "   cc_auth_session " +
            "SET " +
            "   timeout_flg = '1', " +
            "   logout_time = #{logoutTime} " +
            "WHERE " +
            "   user_id = #{userId}; ")
    Integer updExpirationAllSession(@Param("userId") String userId,
                                    @Param("logoutTime") String logoutTime);

    /**
     * Expire session by user id and session id
     * @param userId
     * @param sessionId
     * @param logoutTime
     * @param timeoutFlg
     * @return Integer
     */
    @Insert("UPDATE " +
            "   cc_auth_session " +
            "SET " +
            "   timeout_flg = #{timeoutFlg}, " +
            "   logout_time = #{logoutTime} " +
            "WHERE " +
            "   user_id = #{userId} " +
            "   AND session_id = #{sessionId}; ")
    Integer updExpirationSession(@Param("userId") String userId,
                                 @Param("sessionId") String sessionId,
                                 @Param("logoutTime") String logoutTime,
                                 @Param("timeoutFlg") String timeoutFlg);

}
