package com.org.api.cookie.mapper;

import com.org.api.cookie.model.dataset.RuleDataset;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * RuleMapper
 *
 * @since 2021/04/25
 */
@Mapper
public interface RuleMapper {

    @Update("UPDATE " +
            "   cc_rule " +
            "SET " +
            "   status = #{status} " +
            "WHERE " +
            "   id = #{id} ")
    Integer updRule(@Param("id") String id,
                    @Param("status") String status);

    @Delete("DELETE FROM " +
            "   cc_rule " +
            "WHERE " +
            "   id = #{id} ")
    Integer delRule(@Param("id") String id);

    @Select("SELECT " +
            "   ccr.id, " +
            "   ccr.name, " +
            "   ccr.file_name AS fileName, " +
            "   ccr.status " +
            "FROM " +
            "   cc_rule ccr " +
            "ORDER BY id")
    List<RuleDataset> selRules();

    @Select("SELECT " +
            "   ccr.id, " +
            "   ccr.name, " +
            "   ccr.file_name AS fileName, " +
            "   ccr.status " +
            "FROM " +
            "   cc_rule ccr " +
            "WHERE " +
            "   ccr.status = '0';")
    List<RuleDataset> selEnableRules();

    @Insert("INSERT INTO cc_rule ( " +
            "   id, " +
            "   name, " +
            "   file_name, " +
            "   status " +
            ") VALUES ( " +
            "   #{id}, " +
            "   #{name}, " +
            "   #{fileName}, " +
            "   #{status} " +
            ")")
    Integer insRule(RuleDataset ruleDataset);

}
