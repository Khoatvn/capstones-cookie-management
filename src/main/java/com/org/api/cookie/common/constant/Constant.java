package com.org.api.cookie.common.constant;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Constant
 *
 *
 * @since 2020/06/13
 */
@Getter
@Component
@PropertySource(value = "classpath:constant.properties", encoding = "utf-8")
public class Constant {

    @Value("${AUTH.EXPIRATION}")
    private Integer authExpiration;

}
