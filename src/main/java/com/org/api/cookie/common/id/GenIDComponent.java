package com.org.api.cookie.common.id;

/**
 * GenIDComponent
 *
 *
 * @since 2020/11/14
 */
public interface GenIDComponent {

    String getId(int length, String tableName);

}
