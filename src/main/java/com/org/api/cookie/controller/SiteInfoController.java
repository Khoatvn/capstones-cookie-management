package com.org.api.cookie.controller;

import com.org.api.cookie.model.dataset.ErroMsgDataset;
import com.org.api.cookie.model.dataset.SiteCreateDataset;
import com.org.api.cookie.service.SiteInfoService;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * UserController
 *
 *
 * @since 2020/11/14
 */
@RestController
@Validated
@RequestMapping(value = "/cc/site/api/v1")
public class SiteInfoController {

    private final SiteInfoService siteInfoService;

    public SiteInfoController(SiteInfoService siteInfoService) {
        this.siteInfoService = siteInfoService;
    }


    @GetMapping(value = "/check-exist", produces = MediaType.APPLICATION_JSON_VALUE)
    public ErroMsgDataset checkExistUser(@RequestParam("userId") String userId,
                                         @RequestParam("domain") String domain) {
        return siteInfoService.checkExist(userId, domain);
    }

    @PostMapping(value = "/add-site", produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean addSite(@RequestBody SiteCreateDataset siteCreateDataset) {
        return siteInfoService.addSite(siteCreateDataset);
    }

}
