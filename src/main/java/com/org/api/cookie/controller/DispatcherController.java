package com.org.api.cookie.controller;

import com.org.api.cookie.service.AuthenticateService;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * DispatcherController
 *
 *
 * @since 2021/04/16
 */
@Controller("mainDispatcherController")
@RequestMapping(value = "")
public class DispatcherController implements ErrorController {

    private final AuthenticateService authenticateService;

    public DispatcherController(AuthenticateService authenticateService) {
        this.authenticateService = authenticateService;
    }

    @GetMapping(value = {"", "/", "/dashboard"})
    public String redirectDashboarPage(HttpServletRequest request) {
        return "dashboard";
    }

    @GetMapping(value = {"/user-info"})
    public String redirectUserInfoPage(HttpServletRequest request) {
        return "user-info";
    }

    @GetMapping(value = {"/manage-rule"})
    public String redirectCreateRulePage(HttpServletRequest request) {
        return "manage-rule";
    }

    @GetMapping(value = {"/create-client"})
    public String redirectCreateUserPage(HttpServletRequest request) {
        return "create-client";
    }

    @GetMapping(value = {"/add-site"})
    public String redirectAddSitePage(HttpServletRequest request) {
        return "add-site";
    }

    @GetMapping(value = {"/change-password"})
    public String redirectChangePasswordPage(HttpServletRequest request) {
        return "change-password";
    }

    @GetMapping(value = {"/login"})
    public String redirectLoginPage(HttpServletRequest request) {
        return "login";
    }

    @Override
    public String getErrorPath() {
        return "error";
    }
}
