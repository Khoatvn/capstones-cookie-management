package com.org.api.cookie.controller;

import com.org.api.cookie.service.LogService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * LogController
 *
 *
 * @since 2020/11/14
 */
@RestController
@RequestMapping(value = "/vi/log/api/v1")
public class LogController {

    private final LogService logService;

    public LogController(LogService logService) {
        this.logService = logService;
    }

}
