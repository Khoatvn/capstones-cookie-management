package com.org.api.cookie.controller;

import com.org.api.cookie.model.dataset.RuleDataset;
import com.org.api.cookie.model.dataset.RuleFileDataset;
import com.org.api.cookie.service.RuleService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * RuleController
 *
 *
 * @since 2020/11/14
 */
@RestController
@RequestMapping(value = "/vi/rule/api/v1")
public class RuleController {

    private final RuleService ruleService;

    public RuleController(RuleService ruleService) {
        this.ruleService = ruleService;
    }

    @PostMapping(value = "/create-rule", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    public boolean createRule(RuleDataset ruleDataset) {
        return ruleService.createRule(ruleDataset);
    }

    @GetMapping(value = "/get-all-rule", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RuleDataset> getAllRule() {
        return ruleService.getAllRule();
    }

    @GetMapping(value = "/remove-rule", produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean removeRule(@RequestParam("ruleId") String ruleId) {
        return ruleService.removeRule(ruleId);
    }

    @GetMapping(value = "/update-rule", produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean updateRule(@RequestParam("ruleId") String ruleId,
                              @RequestParam("status") String status) {
        return ruleService.updateRule(ruleId, status);
    }

    @GetMapping(value = "/download-rule", produces = MediaType.APPLICATION_JSON_VALUE)
    public RuleFileDataset downloadRule(@RequestParam("ruleId") String ruleId) {
        return ruleService.downloadRule(ruleId);
    }
}
