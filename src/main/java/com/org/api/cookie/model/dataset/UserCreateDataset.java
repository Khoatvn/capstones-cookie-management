package com.org.api.cookie.model.dataset;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * UserCreateDataset
 *
 *
 * @since 2021/04/18
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserCreateDataset {

    private String id;
    private String username;
    private String password;
    private String fullName;
    private String phoneNumber;
    private String status;
    private String roleId;

}
