package com.org.api.cookie.model.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * SiteRuleEntity
 *
 *
 * @since 2021/04/18
 */
@Data
@Entity
@Table(name = "cc_site_rule")
public class SiteRuleEntity implements Serializable {

    @Id @Column(name = "user_id", length = 7, nullable = false)
    private String userId;
    @Id @Column(name = "rule_id", length = 7, nullable = false)
    private String ruleId;
    @Id @Column(name = "site_id", length = 7, nullable = false)
    private String siteId;

}
