package com.org.api.cookie.model.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * UserEntity
 *
 * @since 2020/11/14
 */
@Data
@Entity
@Table(name = "cc_site_info")
public class SiteInfoEntity implements Serializable {

    @Id @Column(name = "id", length = 7, nullable = false)
    private String id;
    @Id @Column(name = "user_id", length = 7, nullable = false)
    private String userId;
    @Column(name = "domain", length = 250, nullable = false)
    private String serverName;
    @Column(name = "end_point_url", length = 250, nullable = false)
    private String endPointURL;
    @Column(name = "status", length = 1, nullable = false)
    private String status;

}
