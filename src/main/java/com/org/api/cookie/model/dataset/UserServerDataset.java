package com.org.api.cookie.model.dataset;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * UserDataset
 *
 *
 * @since 2020/11/14
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserServerDataset {

    private String id;
    private String username;
    private String password;
    private String fullName;
    private String phoneNumber;
    private String status;
    private String roleId;
    private List<SiteInfoDataset> siteInfoLst;

}
