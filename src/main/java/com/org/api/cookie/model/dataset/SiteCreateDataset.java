package com.org.api.cookie.model.dataset;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * SiteCreateDataset
 *
 *
 * @since 2021/04/18
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SiteCreateDataset {

    private String id;
    private String userId;
    private String domain;
    private String endPointURL;
    private String status;

}
