package com.org.api.cookie.service;

import com.org.api.cookie.common.constant.Constant;
import com.org.api.cookie.common.constant.DataConstant;
import com.org.api.cookie.common.id.GenIDComponent;
import com.org.api.cookie.common.utils.CommonUtils;
import com.org.api.cookie.model.dataset.RuleDataset;
import com.org.api.cookie.model.dataset.RuleFileDataset;
import com.org.api.cookie.mapper.RuleMapper;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Objects;

/**
 * UserServiceImp
 *
 *
 * @since 2020/11/29
 */
@Service
public class RuleService {

    private final Constant constant;
    private final GenIDComponent genIDComponent;
    private final RuleMapper ruleMapper;

    public RuleService(Constant constant,
                       GenIDComponent genIDComponent,
                       RuleMapper ruleMapper) {
        this.constant = constant;
        this.genIDComponent = genIDComponent;
        this.ruleMapper = ruleMapper;
    }
    public boolean createRule(RuleDataset ruleDataset) {
        return true;
    }

    public List<RuleDataset> getAllRule() {
        return ruleMapper.selRules();
    }

    public boolean removeRule(String id) {
        return true;
    }

    public boolean updateRule(String id, String status) {
        return true;
    }

    public RuleFileDataset downloadRule(String ruleId) {
        return null;
    }

    private void activeRules () throws IOException {

    }
}
