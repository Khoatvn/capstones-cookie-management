package com.org.api.cookie.service;

import com.org.api.cookie.common.constant.Constant;
import com.org.api.cookie.mapper.UserMapper;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * LogService
 *
 * @since 2020/12/17
 */
@Service
public class LogService {

    private final Constant constant;
    private final UserMapper userMapper;

    public LogService(Constant constant, UserMapper userMapper) {
        this.constant = constant;
        this.userMapper = userMapper;
    }

}
