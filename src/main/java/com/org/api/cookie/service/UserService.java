package com.org.api.cookie.service;

import com.org.api.cookie.common.constant.Constant;
import com.org.api.cookie.common.id.GenIDComponent;
import com.org.api.cookie.common.utils.CommonUtils;
import com.org.api.cookie.common.utils.EncryptUtils;
import com.org.api.cookie.model.dataset.*;
import com.org.api.cookie.mapper.SiteInfoMapper;
import com.org.api.cookie.mapper.UserMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * UserServiceImp
 *
 *
 * @since 2020/11/29
 */
@Service
public class UserService {

    private final Constant constant;
    private final GenIDComponent genIDComponent;
    private final UserMapper userMapper;
    private final SiteInfoMapper siteInfoMapper;

    public UserService(Constant constant, GenIDComponent genIDComponent, UserMapper userMapper, SiteInfoMapper siteInfoMapper) {
        this.constant = constant;
        this.genIDComponent = genIDComponent;
        this.userMapper = userMapper;
        this.siteInfoMapper = siteInfoMapper;
    }

    public UserDataset getUserInfoComm(String id) {
        return userMapper.selUserInfoComm(id);
    }

    public UserServerDataset getUserInfo(String id) {
        UserServerDataset userServerDataset = userMapper.selUserInfo(id);
        userServerDataset.setSiteInfoLst(siteInfoMapper.selSiteInfoLst(id));
        return userServerDataset;
    }

    @Transactional(rollbackFor = Throwable.class)
    public boolean createUser(UserCreateDataset userCreateDataset) {
        String userId = genIDComponent.getId(7, "cc_user");
        userCreateDataset.setId(userId);
        userCreateDataset.setPassword(EncryptUtils.encryptMD5(userCreateDataset.getPassword()));
        return userMapper.insUser(userCreateDataset) != 0;
    }

    public boolean changePassword(UserPasswordDataset userPasswordDataset) {
        userPasswordDataset.setOldPassword(EncryptUtils.encryptMD5(userPasswordDataset.getOldPassword()));
        userPasswordDataset.setNewPassword(EncryptUtils.encryptMD5(userPasswordDataset.getNewPassword()));
        Integer result = userMapper.updPassword(userPasswordDataset);
        return Objects.nonNull(result) && result != 0;
    }

    public boolean updateUserStatus(String id, String status) {
        boolean result = userMapper.updStatus(id, status) != 0;
        if (result) {
            try {
                // restart apache
                CommonUtils.restartHttpd(constant);
                return true;
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    public ErroMsgDataset checkExist(String username) {
        ErroMsgDataset erroMsgDataset = new ErroMsgDataset();
        erroMsgDataset.setMsg("");
        if (userMapper.selExistUsername(username) != 0) {
            erroMsgDataset.setMsg("Username has been existed!");
        }
        return erroMsgDataset;
    }

    public List<UserServerDataset> getAllUser() {
        return Optional.ofNullable(userMapper.selAllUser())
                .orElse(Collections.emptyList())
                .stream()
                .map(userServer -> {
                    List<SiteInfoDataset> siteInfoLst = siteInfoMapper.selSiteInfoLst(userServer.getId());
                    userServer.setSiteInfoLst(siteInfoLst);
                    return userServer;
                })
                .collect(Collectors.toList());
    }

}
