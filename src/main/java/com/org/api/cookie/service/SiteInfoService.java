package com.org.api.cookie.service;

import com.org.api.cookie.common.constant.Constant;
import com.org.api.cookie.common.id.GenIDComponent;
import com.org.api.cookie.model.dataset.ErroMsgDataset;
import com.org.api.cookie.model.dataset.SiteCreateDataset;
import com.org.api.cookie.mapper.SiteInfoMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * UserServiceImp
 *
 *
 * @since 2020/11/29
 */
@Service
public class SiteInfoService {

    private final Constant constant;
    private final GenIDComponent genIDComponent;
    private final SiteInfoMapper siteInfoMapper;

    public SiteInfoService(Constant constant, GenIDComponent genIDComponent, SiteInfoMapper siteInfoMapper) {
        this.constant = constant;
        this.genIDComponent = genIDComponent;
        this.siteInfoMapper = siteInfoMapper;
    }

    @Transactional(rollbackFor = Throwable.class)
    public boolean addSite(SiteCreateDataset siteCreateDataset) {
        String id = genIDComponent.getId(7, "cc_site_info");
        siteCreateDataset.setId(id);
        return siteInfoMapper.insSiteInfo(siteCreateDataset) != 0;
    }

    public ErroMsgDataset checkExist(String userId, String domain) {
        ErroMsgDataset erroMsgDataset = new ErroMsgDataset();
        erroMsgDataset.setMsg("");
        if (siteInfoMapper.selExistDomain(userId, domain) != 0) {
            erroMsgDataset.setMsg("Domain has been existed!");
        }
        return erroMsgDataset;
    }

}
