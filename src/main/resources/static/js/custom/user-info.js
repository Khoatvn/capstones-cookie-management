/**
 *  File: create-client.js
 *
 */
$(window).on('load', function () {
    USER_INFO.mounted();
});

/**
 * CREATE-USER
 */
const USER_INFO = {
    cookieInfo: {},
    userInfo: {},

    mounted () {
        COMMON_API.getCookieInfo().then(response => {
            COMMON.removeSidebar(response.rid)
            USER_INFO.cookieInfo = response

            return response
        }).then(response => {
            return USER_INFO.getUserInfo()
        }).then(response => {
            USER_INFO.getAllLog()
        })
    },

    getUserInfo () {
        let searchParam = window.location.search
        let userId = searchParam ? searchParam.replace('?id=', '') : ''

        return REST.get('/cc/user/api/v1/get-user-info', {userId: userId})
            .then(response => {
                if (response) {
                    USER_INFO.userInfo = response

                    let btnBlockUnlockUserHtml = ''
                    if (USER_INFO.cookieInfo.rid === CONSTANT.ROLES.ADMIN.id) {
                        let dataUser = response.status === CONSTANT.USER_STATUS.BLOCKED.id ? CONSTANT.USER_STATUS.ACTIVE.id : CONSTANT.USER_STATUS.BLOCKED.id
                        let btnText = response.status === CONSTANT.USER_STATUS.BLOCKED.id ? 'Unlock Client Account' : 'Block Client Account'
                        btnBlockUnlockUserHtml = `<button class="btn-main" style="font-size: 1.4rem;font-weight: bold;" id="btnBlockUser" data-user="${dataUser}">${btnText}</button>`
                    }

                    let statusBadgeClass = ''
                    switch (response.status) {
                        case CONSTANT.USER_STATUS.ACTIVE.id:
                            statusBadgeClass = 'main-badge-success'
                            break
                        case CONSTANT.USER_STATUS.BLOCKED.id:
                            statusBadgeClass = 'main-badge-danger'
                            break
                    }
                    let statusHtml = `<span class="main-badge main-badge-fill ${statusBadgeClass}">
                                        ${COMMON.getValueByKey(CONSTANT.USER_STATUS, 'id', 'name', response.status)}
                                      </span>`

                    let html = `<div style="width: 30%;" id="overviewInfoWrapper">
                                    <div class="info-line">  
                                        <span class="info-left">Full Name: </span>
                                        <span class="info-right">${response.fullName}</span>
                                    </div>
                                    <div class="info-line">  
                                        <span class="info-left">Phone Number: </span>
                                        <span class="info-right">${response.phoneNumber}</span>
                                    </div>
                                    <div class="info-line">  
                                        <span class="info-left">Username: </span>
                                        <span class="info-right">${response.username}</span>
                                    </div>
                                    <div class="info-line">  
                                        <span class="info-left">Status: </span>
                                        <span class="info-right">${statusHtml}</span>
                                    </div>
                                    <div class="info-line">
                                        ${btnBlockUnlockUserHtml}
                                    </div>
                                </div>
                                <div style="width: 70%;">
                                    <p>List Site Information:</p>
                                    <div class="table-responsive table-full-width" style="margin-left: 0; margin-right: 0;">
                                        <table class="table table-hover table-striped">
                                            <thead>
                                                <th>#No</th>
                                                <th>Domain</th>
                                                <th>End Point URL</th>
                                                <th>Status</th>
                                                <th></ht>
                                            </thead>
                                            <tbody id="userList" style="overflow-y: auto; max-height: 75vh;"><!-- List User Information -->`

                    if (response.siteInfoLst && response.siteInfoLst.length !== 0) {
                        response.siteInfoLst.forEach((site, index) => {
                            let siteStatusBadgeClass = ''
                            switch (site.status) {
                                case CONSTANT.USER_STATUS.ACTIVE.id:
                                    siteStatusBadgeClass = 'main-badge-success'
                                    break
                                case CONSTANT.USER_STATUS.BLOCKED.id:
                                    siteStatusBadgeClass = 'main-badge-danger'
                                    break
                            }
                            let siteStatusHtml = `<span class="main-badge main-badge-fill ${siteStatusBadgeClass}">
                                                    ${COMMON.getValueByKey(CONSTANT.SITE_STATUS, 'id', 'name', site.status)}
                                                  </span>`

                            html += `<tr style="width: 100%">
                                        <td>${index + 1}</td>
                                        <td style="word-break: break-word;">${site.domain}</td>
                                        <td style="word-break: break-word;">${site.endPointURL}</td>
                                        <td style="word-break: break-word;">${siteStatusHtml}</td>
                                        <td style="width: 13%;word-break: break-word;">
                                            <button class="btn-main">Manage</button>
                                        </td>
                                    </tr>`
                        })
                    }

                    html += `</tbody>
                          </table>
                       </div>
                    </div>`

                    $('#websiteInfoWrapper').html(html)
                }
            })
    },

    updateUserStatus (_status) {
        let searchParam = window.location.search
        let userId = searchParam ? searchParam.replace('?id=', '') : ''

        let successMsg = `${_status === '1' ? 'Block' : 'Unlock'} user success`
        let errorMsg = `${_status === '1' ? 'Block' : 'Unlock'} user failed`

        REST.get('/vi/user/api/v1/update-user-status', {
            userId: userId,
            status: _status
        })
        .then(response => {
            if (response) {
                USER_INFO.getUserInfo()

                $('#btnBlockUser').attr('data-user', data.userStatus === '1' ? '0' : '1')
                $('#btnBlockUser').text(_status === '1' ? 'Unlock User' : 'Block User')

                COMMON.toastMsg(successMsg, CONSTANT.MSG_TYPE.SUCCESS)
            } else {
                COMMON.toastMsg(errorMsg, CONSTANT.MSG_TYPE.ERROR)
            }
        }).catch(error => {
            if (error.responseText) {
                COMMON.toastMsg( errorMsg, CONSTANT.MSG_TYPE.ERROR)
            } else {
                USER_INFO.getUserInfo()

                $('#btnBlockUser').attr(_status === '1' ? '0' : '1')
                $('#btnBlockUser').text(_status === '1' ? 'Unlock User' : 'Block User')

                COMMON.toastMsg(successMsg, CONSTANT.MSG_TYPE.SUCCESS)
            }
        })
    },

    getAllLog() {
        REST.get('/vi/log/api/v1/get-all-log', {serverName: USER_INFO.serverName})
            .then(response => {
                if (response && response.length !== 0) {
                    $('#logLst').empty()
                    response.forEach((log, index) => {
                        $('#logLst').append(`<tr>
                            <td style="font-size: 1.25rem;background:${index % 2 === 0 ? '#fff' : '#eee'}; overflow: hidden; word-break: break-all">${log}</td></tr>`)
                    })
                }
            })
    }
}


/* Block User Action */
$('#btnBlockUser').click(function () {
    USER_INFO.updateUserStatus($('#btnBlockUser').attr('data-user'))
})