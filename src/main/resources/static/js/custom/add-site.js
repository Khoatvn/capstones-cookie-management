/**
 *  File: add-site.js
 *
 */
$(window).on('load', function () {
    ADD_SITE.mounted();
});

/**
 * ADD-SITE
 */
const ADD_SITE = {
    cookieInfo: {},

    mounted () {
        COMMON_API.getCookieInfo().then(response => {
            COMMON.removeSidebar(response.rid)
            ADD_SITE.cookieInfo = response
        })
    },

    checkExistAndCreate () {
        let userId = ADD_SITE.cookieInfo.uid
        let domain = $('#domain').val()
        let endPointURL = $('#endPointURL').val()

        if (!domain) {
            COMMON.toastMsg('Domain cannot be empty.', CONSTANT.MSG_TYPE.WARNING)
        } else if (!endPointURL) {
            COMMON.toastMsg('End point URL cannot be empty.', CONSTANT.MSG_TYPE.WARNING)
        } else {
            REST.get('/cc/site/api/v1/check-exist', {
                domain: domain,
                userId: userId
            }).then(response => {
                if (response.msg !== '') {
                    COMMON.toastMsg(response.msg, CONSTANT.MSG_TYPE.WARNING)
                }
                return response
            }).then(response => {
                if (response.msg === '') {
                    let site = {
                        userId: userId,
                        domain: domain,
                        endPointURL: endPointURL,
                        status: CONSTANT.SITE_STATUS.ENABLED.id
                    }
                    ADD_SITE.addSite(site)
                }
            })
        }
    },

    addSite (_data) {
        REST.post('/cc/site/api/v1/add-site', _data)
        .then(response => {
            if (response) {
                COMMON.toastMsg('Add new site success.', CONSTANT.MSG_TYPE.SUCCESS)
                ADD_SITE.clearAddSiteForm()
            } else {
                COMMON.toastMsg('Add new site failed.', CONSTANT.MSG_TYPE.ERROR)
            }
        })
        .catch(error => {
            COMMON.toastMsg('Add new site failed.', CONSTANT.MSG_TYPE.ERROR)
        })
    },

    clearAddSiteForm () {
        $('#domain').val('')
        $('#endPointURL').val('')
    }
}

/* Plus www into prefix of domain after focus out */
$('#domain').blur(function () {
    let domain = $('#domain').val()
    if (domain && !domain.startsWith('www.')) {
        $('#domain').val('www.' + domain)
    }
})
$('#endPointURL').blur(function () {
    let endPointURL = $('#endPointURL').val()
    if (endPointURL && !endPointURL.startsWith('www.')) {
        $('#endPointURL').val('www.' + endPointURL)
    }
})

/* [Add Site] */
$('#btnAddSite').click(function () { ADD_SITE.checkExistAndCreate() })

/* [Cancel Add Site] */
$('#btnCancelAddSite').click(function () { ADD_SITE.clearAddSiteForm() })
