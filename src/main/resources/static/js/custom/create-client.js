/**
 *  File: create-client.js
 *
 */
$(window).on('load', function () {
    CREATE_CLIENT.mounted();
});

/**
 * CREATE-USER
 */
const CREATE_CLIENT = {
    cookieInfo: {},

    mounted () {
        COMMON_API.getCookieInfo().then(response => {
            COMMON.removeSidebar(response.rid)
            CREATE_CLIENT.cookieInfo = response
        })
    },

    checkExistAndCreate () {
        let fullName = $('#userFullName').val()
        let phoneNumber = $('#userPhoneNumber').val()
        let username = $('#userUsername').val()
        let password = $('#userPassword').val()
        let role = CONSTANT.ROLES.USER.id

        if (!fullName) {
            COMMON.toastMsg('Full name cannot be empty.', CONSTANT.MSG_TYPE.WARNING)
        } else if (!phoneNumber) {
            COMMON.toastMsg('Phone number cannot be empty.', CONSTANT.MSG_TYPE.WARNING)
        } else if (!username) {
            COMMON.toastMsg('Username cannot be empty.', CONSTANT.MSG_TYPE.WARNING)
        } else if (!password) {
            COMMON.toastMsg('Password cannot be empty.', CONSTANT.MSG_TYPE.WARNING)
        } else {
            REST.get('/cc/user/api/v1/check-exist', {
                username: username
            }).then(response => {
                if (response.msg !== '') {
                    COMMON.toastMsg(response.msg, CONSTANT.MSG_TYPE.WARNING)
                }
                return response
            }).then(response => {
                if (response.msg === '') {
                    let user = {
                        username: username,
                        password: password,
                        fullName: fullName,
                        phoneNumber: phoneNumber,
                        status: CONSTANT.USER_STATUS.ACTIVE.id,
                        roleId: role,
                    }
                    CREATE_CLIENT.createClient(user)
                }
            })
        }
    },

    createClient (_data) {
        REST.post('/cc/user/api/v1/create-client', _data)
        .then(response => {
            if (response) {
                COMMON.toastMsg('Create account success.', CONSTANT.MSG_TYPE.SUCCESS)
                CREATE_CLIENT.clearCreateUserForm()
            } else {
                COMMON.toastMsg('Create account failed.', CONSTANT.MSG_TYPE.ERROR)
            }
        })
        .catch(error => {
            COMMON.toastMsg('Create account failed.', CONSTANT.MSG_TYPE.ERROR)
        })
    },

    clearCreateUserForm () {
        $('#userFullName').val('')
        $('#userPhoneNumber').val('')
        $('#userUsername').val('')
        $('#userPassword').val('')
        $('#userConfirmPassword').val('')
    }
}

/* [Create User] */
$('#btnCreateUser').click(function () { CREATE_CLIENT.checkExistAndCreate() })

/* [Cancel Create User] */
$('#btnCancelCreateUser').click(function () { CREATE_CLIENT.clearCreateUserForm() })

/* [Auto Format Username] */
$('#userUsername').blur(function () {
    let usernameVal = $('#userUsername').val()
    usernameVal = usernameVal.replaceAll(' ', '').toLowerCase().trim()
    $('#userUsername').val(usernameVal)
})